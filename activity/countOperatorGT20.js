db.fruits.aggregate([
    { $match: { stock: { $gte: 20 } }},
    { $count: "Stocks_gt_20" }
])